"""Tests for triggers.utils."""
import json
import os
import types
import unittest
import unittest.mock

import pipeline_tools.utils as utils

import fakes


class TestTrigger(unittest.TestCase):
    """Tests for utils.trigger_pipelines()."""
    def test_trigger_single(self):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        variables = {'cki_project': 'cki-project',
                     'cki_pipeline_branch': 'test_branch',
                     'title': 'commit_title'}

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project')

        utils.trigger_pipelines(gitlab, 'token', [variables])

        pipeline = gitlab.projects['cki-project'].pipelines[0]
        for key, value in variables.items():
            self.assertEqual(pipeline.attributes[key], value)

    def test_required_variables(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        variables = {'cki_project': 'cki-project',
                     'cki_pipeline_branch': 'test_branch',
                     'title': 'commit_title'}

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project')

        for expected in variables:
            missing = variables.copy()
            del missing[expected]
            with self.assertRaises((KeyError, utils.EnvVarNotSetError)):
                utils.trigger_pipelines(gitlab, 'token', [missing])

    @unittest.mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'cki-project'})
    def test_trigger_single_env(self):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        variables = {'cki_pipeline_project': 'cki-pipeline',
                     'cki_pipeline_branch': 'test_branch',
                     'title': 'commit_title'}

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project/cki-pipeline')

        utils.trigger_pipelines(gitlab, 'token', [variables])

        pipeline = gitlab.projects['cki-project/cki-pipeline'].pipelines[0]
        for key, value in variables.items():
            self.assertEqual(pipeline.attributes[key], value)
        self.assertEqual(pipeline.attributes['cki_project'], 'cki-project/cki-pipeline')

    @unittest.mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'cki-project'})
    def test_required_variables_env(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        variables = {'cki_pipeline_project': 'cki-pipeline',
                     'cki_pipeline_branch': 'test_branch',
                     'title': 'commit_title'}

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project/cki-pipeline')

        for expected in variables:
            missing = variables.copy()
            del missing[expected]
            with self.assertRaises(KeyError):
                utils.trigger_pipelines(gitlab, 'token', [missing])

    def test_trigger_multiple_errors(self):
        """
        Test triggering multiple pipelines, some of which invalid. All
        pipelines should be tried.
        """
        variables = [{
            'cki_project': 'cki-project',
            'cki_pipeline_branch': 'branch 1',
            'title': 'title 1',
        }, {
            'cki_project': 'cki-project',
            'cki_pipeline_branch': 'branch 2',
            # 'title' missing
        }, {
            'cki_project': 'cki-project',
            'cki_pipeline_branch': 'branch 3',
            'title': 'title 3',
        }]

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project')

        with self.assertRaises(Exception):
            utils.trigger_pipelines(gitlab, 'token', variables)

        pipelines = gitlab.projects['cki-project'].pipelines.list()
        self.assertEqual(len(pipelines), 2)
        self.assertEqual(pipelines[0].attributes['title'],
                         variables[0]['title'])
        self.assertEqual(pipelines[1].attributes['title'],
                         variables[2]['title'])


class TestCreateCommit(unittest.TestCase):
    """Test cases for utils.create_commit()."""
    def test_created_data(self):
        """Verify the content of created commit looks as expected."""

        commit_text = 'title\n\ncki_pipeline_branch = branch'
        expected_data = {'branch': 'branch',
                         'actions': [],
                         'commit_message': commit_text}

        project = fakes.FakeGitLabProject()
        utils.create_commit(project,
                            {'cki_pipeline_branch': 'branch'},
                            'title')

        self.assertEqual(project.commits[0], expected_data)


class TestArguments(unittest.TestCase):
    """Test cases for argument parsing."""
    def test_store_name_value_pair(self):
        """Verify that key=value arguments are correctly parsed."""
        namespace = types.SimpleNamespace(variables={})
        action = utils.StoreNameValuePair(None, None)
        action(None, namespace, ['key=value', 'key2=value=value2'])
        self.assertEqual(namespace.variables, {
            'key': 'value', 'key2': 'value=value2'
        })
