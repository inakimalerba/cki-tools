"""Fake classes."""
import mock


class FakeGitLabCommits():
    def __init__(self):
        self._commits = []

    def create(self, data):
        self._commits.append(data)

    def __getitem__(self, index):
        return self._commits[index]


class FakePipelineJob():
    def __init__(self, job_id):
        self.attributes = {'id': job_id}


class FakePipelineJobs():
    def __init__(self):
        self._jobs = []
        self.add_job(1)

    def add_job(self, job_id):
        self._jobs.append(FakePipelineJob(job_id))

    def list(self, page=None, per_page=None):
        return self._jobs


class FakePipeline():
    def __init__(self, branch, token, variables, status):
        self.jobs = FakePipelineJobs()
        self.attributes = {'status': status}
        for key, value in variables.items():
            self.attributes[key] = value

        self.branch = branch
        self.token = token
        self.variables = variables


class FakeProjectPipelines():
    def __init__(self):
        self._pipelines = []

    def add_new_pipeline(self, branch, token, variables, status):
        self._pipelines.append(
            FakePipeline(branch, token, variables, status)
        )

    def __getitem__(self, index):
        return self._pipelines[index]

    def list(self, **kwargs):
        kwargs.pop('as_list', None)
        if not kwargs:
            return self._pipelines

        to_return = []
        for pipeline in self._pipelines:
            if pipeline.branch == kwargs.get('ref'):
                to_return.append(pipeline)
            elif pipeline.attributes['status'] == kwargs.get('status'):
                to_return.append(pipeline)

        return to_return


class FakeGitLabProject():
    def __init__(self):
        self.commits = FakeGitLabCommits()
        self.attributes = {'web_url': 'http://web-url'}
        self.pipelines = FakeProjectPipelines()
        self.manager = mock.MagicMock()

    def trigger_pipeline(self, branch, token, variables, status='pending'):
        self.pipelines.add_new_pipeline(branch, token, variables, status)


class FakeGitLab():
    def __init__(self):
        self.projects = {}

    def add_project(self, project_name):
        self.projects[project_name] = FakeGitLabProject()
