import logging
import sys
from . import cancel_pipeline
from . import get_last_successful_pipeline
from . import retrigger

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.DEBUG,
                    stream=sys.stdout)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)

TOOLS = {
    'cancel_pipeline': cancel_pipeline,
    'get_last_successful_pipeline': get_last_successful_pipeline,
    'retrigger': retrigger
}
