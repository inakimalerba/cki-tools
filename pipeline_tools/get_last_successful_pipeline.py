from pipeline_tools import utils


def main(project, args):
    return utils.get_last_successful_pipeline(project, args.cki_pipeline_branch,
                                              args.cki_pipeline_type)
