"""Generic functions and constants"""
import argparse
import json
import time
import os
import re
import logging
from distutils.util import strtobool

import requests

from gitlab import GitlabListError, GitlabGetError

from pipeline_tools.retry import retry


class StoreNameValuePair(argparse.Action):
    """Parse key=value arguments."""
    def __call__(self, parser, namespace, values, option_string=None):
        variables = namespace.variables or {}
        for key_value_pair in values:
            key, value = key_value_pair.split('=', 1)
            variables[key] = value
        namespace.variables = variables


class EnvVarNotSetError(Exception):
    """Requested environment variable is not set."""
    pass


def get_env_var_or_raise(name):
    """
    Retrieve the value of an environment variable. Raise EnvVarNotSetError if
    it's not set.

    Args:
        name:      Name of the variable's value to retrieve.

    Returns:
        Value of the environment variable, if set.

    Raises:
        EnvVarNotSetError if the variable is not set.
    """
    env_var = os.getenv(name)
    if not env_var:
        raise EnvVarNotSetError(f'Environment variable {name} is not set!')
    return env_var


@retry(Exception)
def get_variables(pipeline):
    """
    Get the variables used by the corresponding pipeline.

    Args:
        pipeline: Object representing the pipeline to retrieve variables for

    Returns:
        A dictionary representing pipelines's variables.
    """
    return {var.key: var.value for var in pipeline.variables.list(as_list=False)}


def clean_repository_url(repository):
    """Remove any trailing slashes or .git at the end of repository urls.

    The repository URL is used (1) to refer to raw file contents for the GitLab
    CI YAML includes, and (2) inside of the pipeline code to clone the repo.
    Make sure the URL works for both cases even if the URLs passed on the
    command line would not.
    """
    return re.sub(r'(.git)?/*$', '', repository)


@retry(GitlabListError)
def create_commit(project, trigger, title, pipeline_branch=None):
    """
    Create a commit in the branch where the pipeline will run. It's just used
    for getting an intuitive view using the gitlab interface.

    Args:
        project:         GitLab project to create commit in.
        trigger:         Dictionary with all data to include in the message.
        title:           String to use as the commit message title.
        pipeline_branch: Use this branch instead of cki_pipeline_branch
    """
    commit_message = title + '\n'
    for key, value in trigger.items():
        commit_message += '\n{} = {}'.format(key, value)
    data = {
        'branch': pipeline_branch or trigger['cki_pipeline_branch'],
        'commit_message': commit_message,
        'actions': [],
    }
    if 'tree_name' in trigger and 'PIPELINE_DEFINITION_URL' in os.environ:
        tree_name = trigger['tree_name']
        pipeline_definition_url = clean_repository_url(
            get_env_var_or_raise('PIPELINE_DEFINITION_URL'))
        try:
            project.branches.get(data['branch'])
        except Exception:
            data['start_branch'] = 'master'
            data['actions'].append({
                'action': 'create',
                'file_path': '.gitlab-ci.yml',
                'content': 'include:\n' +
                f'- {pipeline_definition_url}/raw/master/cki_pipeline.yml\n' +
                f'- {pipeline_definition_url}/raw/master/trees/{tree_name}.yml\n'
            })
    project.commits.create(data)


def get_last_successful_pipeline(project, ref, cki_pipeline_type=None):
    """Return the ID of the last successful pipeline of a certain branch.

    Specify a pipeline type to filter on the cki_pipeline_type trigger
    variable.

    Returns None if no such pipeline exists.
    """
    pipelines = retry(GitlabListError)(project.pipelines.list)(
        as_list=False, scope='finished', status='success', ref=ref)

    for pipeline in pipelines:
        vars = get_variables(pipeline)
        if strtobool(vars.get('retrigger', 'False')):
            continue
        if not cki_pipeline_type:
            return pipeline.id
        if vars['cki_pipeline_type'] == cki_pipeline_type:
            return pipeline.id

    return None


def trigger_pipeline(project, trigger_token, trigger, pipeline_branch=None):
    """
    Trigger pipeline.

    Args:
        project:         gitlab.Gitlab.project object representing the project
        trigger_token:   Token to trigger the pipeline with.
        trigger:         Dictionary containing pipeline variables.
    """

    create_commit(project, trigger, trigger['title'], pipeline_branch)
    pipeline = project.trigger_pipeline(pipeline_branch or trigger['cki_pipeline_branch'],
                                        trigger_token,
                                        trigger)
    logging.info('Pipeline "%s" triggered', trigger['title'])
    return pipeline


def ensure_cki_project_variable(triggers):
    """
    Convert cki_pipeline_project to cki_project variable.

    Does nothing if a cki_pipeline_project variable does not exist.

    The value of the cki_project variable is the concatenation of the
    GITLAB_PARENT_PROJECT environment variable and the cki_pipeline_project
    trigger variable.
    """
    if 'cki_pipeline_project' not in triggers:
        return

    parent_project = get_env_var_or_raise('GITLAB_PARENT_PROJECT')
    pipeline_project = triggers.pop('cki_pipeline_project')
    triggers['cki_project'] = f'{parent_project}/{pipeline_project}'


def trigger_pipelines(gitlab_instance, trigger_token, triggers, variables={}):
    """
    Trigger pipelines.

    Args:
        gitlab_instance: gitlab.Gitlab object reporesenting GitLab instance.
        trigger_token:   Token to trigger the pipeline with.
        triggers:        List of dictionaries describing the pipeline to
                         trigger. The dictionary contains pipeline variables.
    """
    errors = []
    for index, trigger in enumerate(triggers):
        try:
            trigger.update(variables)
            ensure_cki_project_variable(trigger)
            project = gitlab_instance.projects.get(trigger['cki_project'])

            # Make sure only strings are passed to make GitLab happy
            for key, value in trigger.items():
                if not isinstance(value, str):
                    trigger[key] = str(value)

            trigger_pipeline(project, trigger_token, trigger)

            logging.info('Pipeline %d/%d for %s triggered',
                         index + 1,
                         len(triggers),
                         trigger['cki_pipeline_branch'])
        except Exception as e:
            errors.append(e)
            logging.exception('Pipeline %d/%d for %s could not be triggered',
                              index + 1,
                              len(triggers),
                              trigger['cki_pipeline_branch'])
    if errors:
        raise errors[0]


def wait_for_pipeline(pipeline):
    time.sleep(30)
    retry(GitlabGetError)(pipeline.refresh)()
    while pipeline.attributes['status'] in ['running', 'pending']:
        time.sleep(60)
        print('.', end='')
        retry(GitlabGetError)(pipeline.refresh)()
    print()
    return pipeline.attributes['status'] == 'success'


def _reset_variable(variables, new_variables, name, message, new_value=None, force=False):
    """Unless overridden on the command line, reset a variable."""
    if (name in variables or force) and name not in (new_variables or {}):
        logging.warning(message)
        if new_value is not None:
            variables[name] = new_value
        elif name in variables:
            del variables[name]


def clean_variables_notifications(variables, new_variables=None):
    """Sanitize variables related to email notifications."""
    _reset_variable(variables, new_variables, 'send_report_to_upstream',
                    'Not sending email to upstream', 'false')
    _reset_variable(variables, new_variables, 'skip_rhcheckpatch',
                    'Disabling rhcheckpatch', 'true', force=True)
    _reset_variable(variables, new_variables, 'mail_to',
                    'Not sending email as mail_to variable is not set')
    _reset_variable(variables, new_variables, 'mail_cc',
                    'Not sending cc email as mail_cc variable is not set')
    _reset_variable(variables, new_variables, 'mail_bcc',
                    'Not sending bcc email as mail_bcc variable is not set')
    _reset_variable(variables, new_variables, 'mail_add_maintainers_to',
                    'Disabling mailing maintainers')


def clean_variables_retrigger(variables, new_variables=None):
    """Sanitize variables related to the pipeline type + mark as retrigger."""
    _reset_variable(variables, new_variables, 'cki_pipeline_type',
                    'Prepending `retrigger-` to cki_pipeline_type ',
                    f'retrigger-{variables.get("cki_pipeline_type", "")}')
    subject = variables.get('subject') or variables.get('title')
    subject = subject or "Empty subject"
    variables['subject'] = f'Retrigger: {subject}'
    variables['title'] = f'Retrigger: {variables["cki_pipeline_type"]}'
    variables['retrigger'] = 'true'


def clean_variables_last_patch_tracking(variables, new_variables=None):
    """Sanitize variables related to last-patch tracking."""
    if variables.get('cki_pipeline_type') == 'patchwork-v1':
        _reset_variable(variables, new_variables, 'last_patch_id',
                        'Disabling Patchwork V1 last-patch tracking')
    if variables.get('cki_pipeline_type') == 'patchwork':
        _reset_variable(variables, new_variables, 'event_id',
                        'Disabling Patchwork last-patch tracking')


def clean_variables(variables, new_variables=None):
    """Sanitize all variables."""
    clean_variables_last_patch_tracking(variables, new_variables)
    clean_variables_notifications(variables, new_variables)
    clean_variables_retrigger(variables, new_variables)
