"""Retrigger pipelines already run."""
import itertools
import re
import uuid

from pipeline_tools import utils, logging


def run_directly_tests(project, pipeline, variables):
    base_url = '{}/-/jobs/{{}}/artifacts/download'.format(
        project.web_url,
    )
    variables['skip_rhcheckpatch'] = 'true'
    variables['skip_build'] = 'true'
    variables['skip_merge'] = 'true'
    variables['skip_createrepo'] = 'true'

    all_jobs = pipeline.jobs.list(all=True)

    # There are multiple stages we might need to check for artifacts here, and
    # we need to do the checks in the right order:
    # - publish: For kernels published outside of GitLab artifacts.
    # - createrepo: For kernels we didn't build ourselves. Watch out that some
    #               pipelines have BOTH publish and createrepo stages which is
    #               why publish needs to be checked first.
    # - build: The rest of the kernels.
    publish_jobs = [j for j in all_jobs if j.attributes['stage'] == 'publish']
    repo_jobs = [j for j in all_jobs if j.attributes['stage'] == 'createrepo']
    build_jobs = [j for j in all_jobs if j.attributes['stage'] == 'build']
    stage_jobs = publish_jobs or repo_jobs or build_jobs

    # Filter out retries. itertools are dumb and we have to sort the jobs
    # first.
    stage_jobs.sort(key=lambda j: j.attributes['name'])
    for name, group in itertools.groupby(stage_jobs,
                                         key=lambda j: j.attributes['name']):
        newest = max(j.attributes['id'] for j in group)
        arch = name.split()[-1]
        variables['ARTIFACT_URL_{}'.format(arch)] = base_url.format(newest)


def create_custom_branch(project, pipeline,
                         pipeline_definition_repository=None, pipeline_definition_branch=None,
                         tree_repository=None, tree_branch=None):
    """Branch from the pipeline commit and adjust GitLab yml include locations.

    To retrigger with a custom repository or branch of the pipeline definition
    or tree, a new branch is created starting at the commit of the given
    pipeline. The GitLab CI configuration is patched to point to the custom
    locations.

    Returns the name of the new branch.
    """
    content = project.files.get('.gitlab-ci.yml', pipeline.sha).decode().decode('utf-8')
    commit_message = f'Retrigger: {pipeline.sha}'
    if pipeline_definition_repository:
        content = re.sub(r'\bhttp.*(?=/raw/[^/]+/cki_pipeline.yml\b)',
                         utils.clean_repository_url(pipeline_definition_repository), content)
    if pipeline_definition_branch:
        content = re.sub(r'[^/]+(?=/cki_pipeline.yml\b)', pipeline_definition_branch, content)
    if tree_repository:
        content = re.sub(r'\bhttp.*(?=/raw/[^/]+/trees/)',
                         utils.clean_repository_url(tree_repository), content)
    if tree_branch:
        content = re.sub(r'[^/]+(?=/trees/)', tree_branch, content)
    branch = f'retrigger-{uuid.uuid4()}'
    project.commits.create({
        'branch': branch,
        'start_sha': pipeline.sha,
        'commit_message': commit_message,
        'actions': [{
            'action': 'update',
            'file_path': '.gitlab-ci.yml',
            'content': content,
        }]
    })
    return branch


def retrigger_pipeline(project, trigger_token, pipeline_id, new_variables,
                       pipeline_definition_repository=None, pipeline_definition_branch=None,
                       tree_repository=None, tree_branch=None,
                       tests_only=False, clone=False):
    """Retrigger an existing pipeline.

    This method is also used by the GitLab pipeline trigger!
    """

    pipeline = project.pipelines.get(pipeline_id)
    variables = utils.get_variables(pipeline)
    variables.update(new_variables or {})
    variables['original_pipeline'] = pipeline.web_url
    custom_locations = (pipeline_definition_repository or
                        pipeline_definition_branch or
                        tree_repository or
                        tree_branch)
    custom_branch = None
    try:
        pipeline_branch = variables['cki_pipeline_branch']
        if not clone:
            if pipeline_definition_repository:
                variables['pipeline_definition_repository_override'] = \
                    utils.clean_repository_url(pipeline_definition_repository)
            if pipeline_definition_branch:
                variables['pipeline_definition_branch_override'] = pipeline_definition_branch
            if custom_locations:
                custom_branch = create_custom_branch(
                    project, pipeline,
                    pipeline_definition_repository, pipeline_definition_branch,
                    tree_repository, tree_branch)
                logging.info('Created branch %s in %s', custom_branch, project.web_url)
                pipeline_branch = custom_branch
            utils.clean_variables_notifications(variables, new_variables)
            utils.clean_variables_retrigger(variables, new_variables)
        else:
            if custom_locations:
                raise Exception('Unable to clone with custom locations')
            utils.clean_variables_last_patch_tracking(variables, new_variables)
            print('Trigger variables:')
            for key, value in variables.items():
                print(f'  {key}: {value}')
            if input('Are you sure that you want to submit a new pipeline with '
                     'these variables (enter upper case yes)? ') != 'YES':
                raise Exception('Aborting...')

        if tests_only:
            run_directly_tests(project, pipeline, variables)

        new_pipeline = utils.trigger_pipeline(project, trigger_token, variables,
                                              pipeline_branch)
    finally:
        if custom_branch:
            project.branches.delete(custom_branch)
            logging.info('Deleted branch %s in %s', custom_branch, project.web_url)
    return new_pipeline


def main(project, args):
    new_pipeline = retrigger_pipeline(project, args.trigger_token,
                                      args.pipeline_id, args.variables,
                                      args.pipeline_definition_repository, args.pipeline_definition_branch,
                                      args.tree_repository, args.tree_branch,
                                      args.tests_only, args.clone)
    pipeline_url = '{}/pipelines/{}'.format(project.attributes['web_url'],
                                            new_pipeline.attributes['id'])
    logging.info('Pipeline url %s', pipeline_url)
    if args.wait:
        if not utils.wait_for_pipeline(new_pipeline):
            return new_pipeline.attributes['status']
